**This is the home page of [this documentation](http://wonderland.eode.studio/docs/net-_introduction.html)**

### Version
> In dev, solution works but it was not tested yet in a complete project
> Serialization and Networking are strong.
> SimpleNet is tested and debugged but do use carefully: it has been tested only with simple scenes. Its code is open source.

### Next major features
- `Ready to deploy` (searching for a viable option)
- `High level components` (like inventory, doors system...)

### Dependencies
- `Common`

## Is it High Level ?
If you search for high level features, see [SimpleNet](/docs/net-SNIntroduction.html). If you want a complete solution for networking and serialization, keep reading.

## Motivation
### A strong serialization system
`Wonderland.Net` provides a strong serialization system using `Reflection`. It can automatically transforms complexe objects to byte arrays recursively. The goal is to create the simplest API like : `MyPacket(MyObject)`. Additionally, all objects serialization and deserialization can be overrited to optimize and customize as you like.

### A network system based on Awaiters
It's most modern and simple. Just send, wait response or create dialogs.

### P2P and Host
P2P or Host mode are define by scene.

### Web sockets
A server can have Normal and Web sockets at same time.

### Just to create an alternative solution
If you want more control or hesitating on coding your own based on C# sockets, I made this module for you.

### Take a local scene and click on a button to create an online scene
Sorry, it can't. But I have tried coding that. At the end, it appears it is too hard to finish all researchs on certain points with a small team of one dev :D. But that was cool and works good. Maybe, one day the `Democracy` module will come back.

Currently, for this first version, `SimpleNet` offers a good example of an implementation of a simple network.

