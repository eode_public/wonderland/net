﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EODE.Wonderland.Networking;
using UnityEngine;

[assembly: UnityEngine.Scripting.AlwaysLinkAssembly]

namespace EODE.Wonderland {
    static public class NetBuildInjector {
        [RuntimeInitializeOnLoadMethod]
        static public void InitInjections() {
            if (Peer.__tcpClientInjection != null) return;

            // Common
            Peer.__tcpClientInjection = CreateTcpClient;
            TcpServer.__DebugLogErr = DebugLogErr;
            TcpServer.__DebugLogWarn = DebugLogWarn;
            TcpServer.__DebugLog = DebugLog;

#if WONDERLAND_SOCKETSTATS || UNITY_EDITOR
            // Debugs
            CloudDebug.Enabled = true;
            UdpDebug.Enabled = true;

            // Network
            EODE.Wonderland.Networking.Network.__statsInjection = true;

            // Peer
            Peer.__statsInjection = true;
#endif
#if WONDERLAND_LATENCY_SIM || UNITY_EDITOR
            // Peer
            Peer.__latencySimInjection = true;
#endif

            UdpDebug.InjectionDone = true;
        }

        static public EODE.Wonderland.Networking.ITcpClient CreateTcpClient(string address, int port) {
#if UNITY_WEBGL && !UNITY_EDITOR
            return new EODE.Wonderland.Networking.WebTcpClient(address, port);
#else
            return new EODE.Wonderland.Networking.TcpClient(address, port);
#endif
        }

        static public void DebugLogErr(string value) {
#if UNITY_EDITOR
            Debug.LogError(value);
#else
            System.Console.Error.WriteLine("/!\\ " + System.DateTime.Now.ToString() + " | " + value);
#endif
        }

        static public void DebugLogWarn(string value) {
#if UNITY_EDITOR
            Debug.LogWarning(value);
#else
            System.Console.WriteLine("[?] " + System.DateTime.Now.ToString() + " | " + value);
#endif
        }

        static public void DebugLog(string value) {
#if UNITY_EDITOR
            Debug.Log(value);
#else
            System.Console.WriteLine(System.DateTime.Now.ToString() + " | " + value);
#endif
        }
    }
}